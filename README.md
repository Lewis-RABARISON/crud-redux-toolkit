### Requirement
- json server

### Installation
```
npm install -g json-server
```

### Launching
```
json-server --watch data.json
```
