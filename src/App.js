import React from "react";
import "./App.css";
import UsersPage from "./pages/UsersPage";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AddPage from "./pages/AddPage";
import EditPage from "./pages/EditPage";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<UsersPage />} />
          <Route path="/utilisateur/:id" element={<EditPage />} />
          <Route path="/ajout-utilisateur" element={<AddPage />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
