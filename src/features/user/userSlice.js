import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  isLoading: false,
  users: [],
  user: {},
  isSuccess: false,
  error: "",
};

export const getAllUsers = createAsyncThunk(
  "users/list",
  async (_, _thunkAPI) => {
    try {
      return await axios
        .get("http://localhost:3000/users")
        .then((response) => response.data);
    } catch (error) {
      return error;
    }
  }
);

export const addUser = createAsyncThunk("user/add", async (data, _thunkAPI) => {
  try {
    return await axios.post("http://localhost:3000/users", data);
  } catch (error) {
    return error;
  }
});

export const editUser = createAsyncThunk("user/edit", async (id, _thunkAPI) => {
  try {
    return await axios
      .get(`http://localhost:3000/users/${id}`)
      .then((response) => response.data);
  } catch (error) {
    return error;
  }
});

export const updateUser = createAsyncThunk(
  "user/update",
  async (data, _thunkAPI) => {
    const { id } = data;

    try {
      return await axios.patch(`http://localhost:3000/users/${id}`, data);
    } catch (error) {
      return error;
    }
  }
);

export const deleteUser = createAsyncThunk(
  "user/delete",
  async (id, _thunkAPI) => {
    try {
      return await axios.delete(`http://localhost:3000/users/${id}`);
    } catch (error) {
      return error;
    }
  }
);

const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    resetUser: (state) => initialState,
  },
  extraReducers: {
    [getAllUsers.pending]: (state) => {
      state.isLoading = true;
    },
    [getAllUsers.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.users = action.payload;
      state.isSuccess = true;
    },
    [getAllUsers.rejected]: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [editUser.pending]: (state) => {
      state.isLoading = true;
    },
    [editUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.user = action.payload;
      state.isSuccess = true;
    },
    [editUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [addUser.pending]: (state) => {
      state.isLoading = true;
    },
    [addUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.users = state.users.push(action.payload);
    },
    [addUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [updateUser.pending]: (state) => {
      state.isLoading = true;
    },
    [updateUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;

      let index = state.users
        .map((u) => u.id)
        .findIndex((u) => u.id === action.payload.id);
      state.users[index].firstname = action.payload.firstname;
    },
    [updateUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    [deleteUser.pending]: (state) => {
      state.isLoading = true;
    },
    [deleteUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.users = state.users.filter((u) => u.id !== action.payload.id);
    },
    [deleteUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});

export const { resetUser } = userSlice.actions;
export default userSlice.reducer;
