import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addUser } from "../features/user/userSlice";

const AddPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { users, isSuccess } = useSelector((state) => state.user);

  const [user, setUser] = useState({
    id: users.length > 0 ? users.length + 1 : 1,
    firstname: "",
  });

  const handleChange = ({ currentTarget: { name, value } }) => {
    setUser({ ...user, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addUser(user));

    if (isSuccess) {
      navigate("/");
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="firstname"
          value={user.firstname}
          onChange={handleChange}
        />
        <button type="submit">Enregistrer</button>
      </form>
    </div>
  );
};

export default AddPage;
