import React, { useEffect } from "react";
import { deleteUser, getAllUsers } from "../features/user/userSlice";
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const UsersPage = () => {
  const { isSuccess, error, users } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [usersList, setUsersList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getAllUsers());

    if (isSuccess) {
      setUsersList((prevUsers) => ({
        ...prevUsers,
        users,
      }));
    }
  }, [isSuccess, dispatch, users]);

  const addUser = () => {
    navigate("/ajout-utilisateur");
  };

  const handleDelete = (id) => {
    dispatch(deleteUser(id));
    if (isSuccess) {
      navigate("/");
    }
  }

  return (
    <>
      <button onClick={addUser}>Ajouter un utilisateur</button>
      <table>
        <thead>
          <tr>
            <th>Prenom</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.length > 0 &&
            users.map((user) => (
              <tr key={user.id}>
                <td>{user.firstname}</td>
                <td>
                  <Link to={`/utilisateur/${user.id}`}>Editer</Link>
                  <button onClick={() => handleDelete(user.id)}>Supprimer</button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
};

export default UsersPage;
