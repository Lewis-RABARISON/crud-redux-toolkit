import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { editUser, updateUser } from "../features/user/userSlice";

const EditPage = () => {
  const [userEdited, setUserEdited] = useState({
    firstname: "",
  });

  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user, isSuccess } = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(editUser(id));
    setUserEdited({ firstname: user.firstname });
  }, [dispatch, id]);

  const handleChange = ({ currentTarget: { name, value } }) => {
    setUserEdited({ ...userEdited, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let data = { id, firstname: userEdited.firstname };
    dispatch(updateUser(data));

    if (isSuccess) {
      navigate("/");
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="firstname"
          value={userEdited.firstname}
          onChange={handleChange}
        />
        <button type="submit">Enregistrer</button>
      </form>
    </div>
  );
};

export default EditPage;
