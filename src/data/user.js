let dataUsers = [
  {
    id: 1,
    firstname: "Lewis",
  },
  {
    id: 2,
    firstname: "Jean",
  },
  {
    id: 3,
    firstname: "Kun",
  },
  {
    id: 4,
    firstname: "Kygo",
  },
];

export default dataUsers;
